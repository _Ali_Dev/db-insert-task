--Choose one of your favorite films and add it to the "film" table. Fill in rental rates with 4.99 and rental durations with 2 weeks.
INSERT INTO film (title, description, release_year, language_id, rental_rate, rental_duration, length)
VALUES ('Meg', 'The biggest shark', 2018, 1, 4.99, 14, 113);

--Add the actors who play leading roles in your favorite film to the "actor" and "film_actor" tables (three or more actors in total).
INSERT INTO actor (first_name, last_name)
VALUES
    ('Jason', 'Statham'),
    ('Li', 'Bingbing'),
    ('Jessica', 'McNamee')

SELECT film_id FROM film WHERE title = 'Meg';

INSERT INTO film_actor (film_id, actor_id)
VALUES
    (1004, 201),
    (1004, 202),
    (1004, 203);

--Add your favorite movies to any store's inventory.
INSERT INTO inventory (film_id, store_id)
VALUES
    (1004, 1);